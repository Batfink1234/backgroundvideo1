﻿using System.Diagnostics;
using Xamarin.Forms;

namespace BackgroundVideo1
{
    public partial class MainPage : ContentPage
	{
		public MainPage()
		{
			InitializeComponent();
            video.OnFinishedPlaying = () => { Debug.WriteLine("Video Finished"); };
        }
	}
}
